import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    currentUser: null,
    uID: null,
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setAccessToken: (state, action) => {
            state.value = action.payload;
        },

        setInfoUser: (state, action) => {
            state.currentUser = action.payload.currentUser;
            state.uID = action.payload.uID
        }
    }
});

export const {
    setAccessToken,
    setInfoUser
} = authSlice.actions

export const selectAccessToken = state => state.auth.value
export const selectCurrentUser = state => state.auth.currentUser
export const selectUID = state => state.auth.uID

export default authSlice.reducer