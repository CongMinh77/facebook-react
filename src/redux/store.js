import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";

import userReducer from '../redux/authSlice'

export default configureStore({
    reducer: {
        auth: userReducer
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
})