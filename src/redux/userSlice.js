import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    userName: [],
    userPassword: null
}

const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        setUserSuccess: (state, action) => {
            state.userName = action.payload.userName
            state.userPassword = action.payload.userPassword
        },

        setUserFailure: (state) => {
            state.userName = null
            state.userPassword = null    
        }
    }
});

export const {
    setUserSuccess
} = userSlice.actions

export const selectUserName = state => state.user.userName
export default userSlice.reducer