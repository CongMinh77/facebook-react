import React from "react";
import { Container } from "react-bootstrap";
import { Layout } from "../components/Layout";

export default function NotFoundPage() {
  return (
    <Layout>
      <Container>
        <h1 className="text-center mt-5">Page Not Found - 404</h1>
      </Container>
    </Layout>
  );
}
