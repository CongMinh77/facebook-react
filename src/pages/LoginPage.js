import React from "react";
import { Button, Col, Container, Form, Row, Card } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import { useState } from "react";
import { signInWithEmailAndPassword, signInWithPopup } from "firebase/auth";
import { FcGoogle } from "react-icons/fc";
import { Layout } from "../components/Layout";
import { auth, providerGoogle } from "../utils/init-firebase";

export default function LoginPage() {
  const history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    signInWithEmailAndPassword(auth, email, password)
      .then((response) => {
        console.log(response);
        history.push("/profile");
      })
      .catch((error) => {
        const errorMessage = error.message;
        console.log("An error occurred: ", errorMessage);
      });
  };

  const handleSignInWithGoogle = (e) => {
    e.preventDefault();
    signInWithPopup(auth, providerGoogle)
      .then((response) => {
        console.log(response);
        history.push("/profile");
      })
      .catch((error) => {
        const errorMessage = error.message;
        console.log("An error occurred: ", errorMessage);
      });
  };

  return (
    <Layout>
      <Container className="mt-5">
        <Card className="m-auto" style={{ width: "30rem" }}>
          <Card.Header className="text-center" as="h2">
            Login
          </Card.Header>
          <Form onSubmit={handleSubmit} className="p-3">
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                type="email"
                placeholder="name@example.com"
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                type="password"
                placeholder="password"
              />
            </Form.Group>
            <Container className="border-bottom p-0">
              <Row>
                <Col>
                  <Button className="mt-2 w-100" type="submit" fontSize="md">
                    Sign in
                  </Button>
                </Col>
              </Row>
              <Col className="mt-2">
                <Link to={"/register"}>
                  <h6 className="text-center">Register</h6>
                </Link>
              </Col>
            </Container>
            <h6 className="text-center m-0 pt-2">or</h6>
            <Col>
              <div>
                <Button
                  className="mt-2 w-100 d-flex justify-content-center align-items-center"
                  type="submit"
                  variant="outline-danger"
                  fontSize="md"
                  onClick={handleSignInWithGoogle}
                >
                  <FcGoogle />
                  <p className="mb-0" style={{ marginLeft: 6 }}>
                    Sign in with Google
                  </p>
                </Button>
              </div>
            </Col>
          </Form>
        </Card>
      </Container>
    </Layout>
  );
}
