import React from "react";
import { Card, Container } from "react-bootstrap";
import { Layout } from "../components/Layout";
import logoFB from "../assets/images/download.png";

export default function AboutPage() {
  return (
    <Layout>
      <Container>
        <h2 className="text-center mt-5 mb-3">About React App</h2>
        <Card className="m-auto" style={{ width: "35rem" }}>
          <Card.Img variant="top" src={logoFB} />
          <Card.Body>
            <Card.Subtitle className="mb-2 text-muted text-center">Design by: NCM</Card.Subtitle>
          </Card.Body>
        </Card>
      </Container>
    </Layout>
  );
}
