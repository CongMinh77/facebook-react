import React from "react";
import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Layout } from "../components/Layout";

export default function DashboardPage() {
  return (
    <Layout>
      <Container className="justify-content-center">
        <h2 className="text-center mt-5">Dashboard Page</h2>
        <Link to={"/user"}>
          <Button variant="primary" onClick={() => console.log("Primary")}>
            <h6 className="text-center m-0">List User</h6>
          </Button>
        </Link>
      </Container>
    </Layout>
  );
}
