import React from "react";
import { useSelector } from "react-redux";
import { Button, Container, Col, Row, Navbar, Card, Form, Image } from "react-bootstrap";
import { FaCamera, FaPlusCircle, FaPen, FaVideo, FaImages, FaFlag } from "react-icons/fa";
import { Layout } from "../components/Layout";
import { selectCurrentUser, selectUID, selectAccessToken } from "../redux/authSlice";
import default_avatar from "../assets/images/default-avt-pink.webp";
import "../assets/css/profile.css";

export default function ProfilePage() {
  const user = useSelector(selectCurrentUser);
  const uID = useSelector(selectUID);
  const accessToken = useSelector(selectAccessToken);
  return (
    <Layout>
      <Container>
        <Container>
          <Container className="background align-content-end justify-content-end" style={{ height: "20rem" }}>
            <Col className="pt-2">
              <Button className="d-flex align-items-center" variant="primary">
                <FaCamera />
                <p className="m-0 pl-2">Edit Cover Photo</p>
              </Button>
            </Col>
          </Container>
          <Container className="mt-3">
            <Container className="d-flex align-items-center pb-3 border-bottom">
              <Image roundedCircle={true} src={default_avatar} style={{ width: "12rem" }} />
              <Container>
                <h2 className="m-0">{user}</h2>
              </Container>
              <Container className="d-flex flex-column align-items-end">
                <Col>
                  <Button className="d-flex align-items-center" variant="primary">
                    <FaPlusCircle />
                    <p className="m-0 pl-2">Add to story</p>
                  </Button>
                </Col>
                <Col className="mt-2">
                  <Button className="d-flex align-items-center" variant="light">
                    <FaPen />
                    <p className="m-0 pl-2">Edit profile</p>
                  </Button>
                </Col>
              </Container>
            </Container>
            <Container fluid>
              <Navbar expand="lg" bg="light" variant="light">
                <Container>
                  <Col>
                    <Button className="mr-2" variant="light" size="lg">
                      Posts
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      About
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      Friends
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      Photos
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      Videos
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      Check-Ins
                    </Button>{" "}
                    <Button className="mr-2" variant="light" size="lg">
                      More
                    </Button>
                  </Col>
                  <Button variant="outline-dark" size="lg">
                    ...
                  </Button>
                </Container>
              </Navbar>
            </Container>
          </Container>
        </Container>
        <Container className="mt-3">
          <Row>
            <Col xs={4}>
              <Container>
                <Card>
                  <Card.Header as="h5">Intro</Card.Header>
                  <Container fluid="md">
                    <label>UID:</label>
                    <h6>
                      <small>{uID}</small>
                    </h6>
                    <label>Access Token</label>
                    <textarea defaultValue={accessToken} rows="5" />
                  </Container>
                </Card>
              </Container>
            </Col>
            <Col xs={8}>
              <Container fluid>
                <Card>
                  <Container className="d-flex align-items-center mt-2 pb-2 border-bottom">
                    <Image className="rounded-circle" src={default_avatar} style={{ width: "3.5rem" }} />
                    <Form.Group className="w-100">
                      <Form.Control type="text" placeholder="What's on your mind?" />
                    </Form.Group>
                  </Container>
                  <Container className="d-flex justify-content-center mt-2 mb-3">
                    <Row className="w-100">
                      <Col>
                        <Button className="d-flex align-items-center m-auto" variant="light">
                          <FaVideo />
                          <p className="m-0 pl-2">Live video</p>
                        </Button>
                      </Col>
                      <Col>
                        <Button className="d-flex align-items-center m-auto" variant="light">
                          <FaImages />
                          <p className="m-0 pl-2">Photo/Video</p>
                        </Button>
                      </Col>
                      <Col>
                        <Button className="d-flex align-items-center m-auto" variant="light">
                          <FaFlag />
                          <p className="m-0 pl-2">Life event</p>
                        </Button>
                      </Col>
                    </Row>
                  </Container>
                </Card>
              </Container>
            </Col>
          </Row>
        </Container>
      </Container>
    </Layout>
  );
}
