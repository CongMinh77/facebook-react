import React, { useEffect, useState } from "react";
import { Container, Table, Button, Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { FaUserPlus } from "react-icons/fa";
import { Layout } from "../components/Layout";
import { selectCurrentUser } from "../redux/authSlice";
import { all } from "../services/user";

export default function UserPage() {
  const user = useSelector(selectCurrentUser);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    setUsers(await all());
  };

  return (
    <Layout>
      <Container>
        {user && (
          <Container>
            <Container className="d-flex mb-2 mt-2">
              <Col>
                <h3 className="m-0">List</h3>
              </Col>
              <Button className="justify-content-end" variant="success" onClick={() => console.log("Primary")}>
                <FaUserPlus />
              </Button>
            </Container>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Username</th>
                  <th>Password</th>
                </tr>
              </thead>
              <tbody>
                {users.length > 0 &&
                  users.map((user) => (
                    <tr key={user.id}>
                      <td>{user.id}</td>
                      <td>{user.userName}</td>
                      <td>{user.password}</td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          </Container>
        )}
      </Container>
    </Layout>
  );
}
