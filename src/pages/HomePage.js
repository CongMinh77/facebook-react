import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import { useSelector } from "react-redux";
import { Layout } from "../components/Layout";
import { selectCurrentUser } from "../redux/authSlice";

export default function HomePage() {
  const user = useSelector(selectCurrentUser);

  return (
    <Layout>
      <Container fluid>
        <Row>
          <Col className="text-center mt-3">Welcome to {user} page</Col>
        </Row>
      </Container>
    </Layout>
  );
}
