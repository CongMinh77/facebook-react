import React from "react";
import { useState } from "react";
import { Button, Col, Container, Form, Row, Card } from "react-bootstrap";
import { IoChevronBackOutline } from "react-icons/io5";
import { useHistory } from "react-router-dom";
import { Layout } from "../components/Layout";
import { createUserWithEmailAndPassword } from "firebase/auth";
import { auth } from "../utils/init-firebase";

export default function RegisterPage() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null);

  const history = useHistory();

  const handleRegister = async (e) => {
    e.preventDefault();
    console.log(email, password);
    createUserWithEmailAndPassword(auth, email, password)
      .then((response) => {
        console.log(response);
        history.push("/login");
      })
      .catch((error) => {
        const errorMessage = error.message;
        setError(error.message);
        console.log("An error occurred: ", errorMessage);
      });
  };

  return (
    <Layout>
      <Container className="mt-5">
        <Card className="m-auto" style={{ width: "30rem" }}>
          <Container className="position-relative p-0">
            <Button
              className="position-absolute h-100 m-0"
              variant="outline-dark"
              onClick={() => {
                history.goBack();
              }}
            >
              <IoChevronBackOutline className="" />
            </Button>
            <Card.Header className="text-center" as="h2">
              Register
            </Card.Header>
          </Container>
          <Form onSubmit={handleRegister} className="p-3">
            <Form.Group className="mb-3">
              <Form.Label>Email</Form.Label>
              <Form.Control
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                type="email"
                placeholder="name@example.com"
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Label>Password</Form.Label>
              <Form.Control
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                type="password"
                placeholder="password"
              />
            </Form.Group>
            <Container className="p-0">
              <Row>
                <Col>
                  <Button className="mt-2 w-100" type="submit" fontSize="md">
                    Register
                  </Button>
                </Col>
              </Row>
            </Container>
          </Form>
          <Container className="text-danger mb-2">{error}</Container>
        </Card>
      </Container>
    </Layout>
  );
}
