import { Provider } from 'react-redux';
import store from './redux/store';
import AppRoute from './routes/AppRoute';

function App() {
  return (
    <Provider store={store}>
      <AppRoute/>
    </Provider>
  );
}

export default App;
