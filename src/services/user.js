import axios from "../utils/axios";

export const all = async () => {
  try {
    const response = await axios.get("Users");
    return response.data;
  } catch (e) {
    console.log(e);
  }
};

export const create = async (data) => {
  try {
    const response = await axios.post("Users", data);
    return response.data;
  } catch (e) {
    console.log(e);
  }
};
