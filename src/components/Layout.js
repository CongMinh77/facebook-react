import React from "react";
import { Container } from "react-bootstrap";
import { AppNavbar } from "./Navbar";

export const Layout = ({ children }) => {
  return (
    <Container fluid className="m-0 p-0">
      <AppNavbar />
      {children}
    </Container>
  );
};
