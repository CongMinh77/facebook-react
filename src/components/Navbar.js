import React from "react";
import { useEffect } from "react";
import { Navbar, Image, Container, Col } from "react-bootstrap";
import { FaSignOutAlt, FaInfo, FaHome } from "react-icons/fa";
import { BsFillGrid3X3GapFill } from "react-icons/bs";
import { useSelector, useDispatch } from "react-redux";
import { signOut, onAuthStateChanged } from "firebase/auth";

import { auth } from "../utils/init-firebase";
import { setAccessToken, selectAccessToken, setInfoUser } from "../redux/authSlice";
import NavLink from "./NavLink";
import default_avatar from "../assets/images/default-avt-pink.webp";

export const AppNavbar = () => {
  const user = useSelector(selectAccessToken);
  const dispatch = useDispatch();

  useEffect(() => {
    onAuthStateChanged(auth, (user) => {
      if (user) {
        dispatch(setAccessToken(user.refreshToken));
        dispatch(
          setInfoUser({
            currentUser: user.email,
            uID: user.uid,
          })
        );
      } else {
        dispatch(setAccessToken(undefined));
        dispatch(
          setInfoUser({
            currentUser: null,
            uID: null,
          })
        );
      }
    });
  }, [dispatch]);

  return (
    <Navbar bg="light" sticky="top">
      <Container>
        <Col sm={8}>
          <NavLink to="/" name={<FaHome />} size="lg" />
          <NavLink to="/about" name={<FaInfo />} size="lg" />
        </Col>
        <Col sm={4} className="d-flex justify-content-end">
          {!user && <NavLink to="/login" name="Login" />}
          {user && (
            <NavLink
              to="/profile"
              name={<Image roundedCircle={true} src={default_avatar} style={{ width: "1.2rem" }} />}
            />
          )}
          {user && <NavLink to="/dashboard" name={<BsFillGrid3X3GapFill />} />}
          {user && (
            <NavLink
              to="/logout"
              name={<FaSignOutAlt />}
              onClick={async (e) => {
                e.preventDefault();

                await signOut(auth)
                  .then(() => {
                    console.log("user signed out");
                  })
                  .catch((error) => {
                    console.log("error", error);
                  });
              }}
            />
          )}
        </Col>
      </Container>
    </Navbar>
  );
};
