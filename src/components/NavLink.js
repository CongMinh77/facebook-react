import React from "react";
import { NavLink as Link, useLocation } from "react-router-dom";
import { Button } from "react-bootstrap";

export default function NavLink({ to, name, ...rest }) {
  const location = useLocation();

  const isActive = location.pathname === to;

  return (
    <Link to={to}>
      <Button variant={isActive ? "outline-dark" : null} {...rest}>
        {name}
      </Button>
    </Link>
  );
}
