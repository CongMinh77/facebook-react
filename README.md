### Description

- Chương trình gồm các màn hình: **Home, About, Dashboard, Profile, Login, Register.**

- Màn Login: được làm dạng single-sign on với Firebase, sau khi login xong lưu token xuống local storage.

- Trang Home hiển thị username người đăng nhập hiện tại và nếu đăng nhập nó sẽ gọi API để demo ra một vài username từ trang **https://fakerestapi.azurewebsites.net/api/v1/Users.**

- Trang About chỉ để demo, trang này không cần login vẫn có thể vào.

- Sau khi đã login thì chỗ Avatar có hiển thị thêm 3 mục Dashboard, Profile, Logout.

- Trang Profile clone một ít trang profile của FB, Dashboard thì để demo.

- Web trên được deploy trên Firebase hosting: **https://facebook-react-cmse7en.web.app**

### Run local

```
cp .env.example .env

npm i
```
